# DOCS

## Keyboard Helper

a pdf file containing the USB hid files


## keybrd.h

when the code is generated after the STM CUBE_MX, some parameters need to be adjusted for use as keyboard.

`Middlewares/ST/STM32_USB_Device_Library/Class/HID/Src/usbd_hid.c`

```diff
USB_DESC_TYPE_INTERFACE,                            /* bDescriptorType: Interface descriptor type */
0x00,                                               /* bInterfaceNumber: Number of Interface */
0x00,                                               /* bAlternateSetting: Alternate setting */
0x01,                                               /* bNumEndpoints */
0x03,                                               /* bInterfaceClass: HID */
0x01,                                               /* bInterfaceSubClass : 1=BOOT, 0=no boot */
- 0x02,                                               /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */
+ 0x01,                                               /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */

```


at line   313(https://gitlab.com/-/ide/project/dgoo2308/mama_keyboard/tree/master/-/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Src/usbd_hid.c/#L313) of `Middlewares/ST/STM32_USB_Device_Library/Class/HID/Src/usbd_hid.c` change the USB HID Descriptor according [keybrd.h](DOCS/keybrd.h)

```c
__ALIGN_BEGIN static uint8_t HID_MOUSE_ReportDesc[HID_MOUSE_REPORT_DESC_SIZE] __ALIGN_END =
{
		 0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
		    0x09, 0x06,                    // USAGE (Keyboard)
		    0xa1, 0x01,                    // COLLECTION (Application)
		    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
		    0x19, 0xe0,                    //   USAGE_MINIMUM (Keyboard LeftControl)
		    0x29, 0xe7,                    //   USAGE_MAXIMUM (Keyboard Right GUI)
		    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
		    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
		    0x75, 0x01,                    //   REPORT_SIZE (1)
		    0x95, 0x08,                    //   REPORT_COUNT (8)
		    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
		    0x95, 0x01,                    //   REPORT_COUNT (1)
		    0x75, 0x08,                    //   REPORT_SIZE (8)
		    0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
		    0x95, 0x05,                    //   REPORT_COUNT (5)
		    0x75, 0x01,                    //   REPORT_SIZE (1)
		    0x05, 0x08,                    //   USAGE_PAGE (LEDs)
		    0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
		    0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
		    0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
		    0x95, 0x01,                    //   REPORT_COUNT (1)
		    0x75, 0x03,                    //   REPORT_SIZE (3)
		    0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
		    0x95, 0x06,                    //   REPORT_COUNT (6)
		    0x75, 0x08,                    //   REPORT_SIZE (8)
		    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
		    0x25, 0x65,                    //   LOGICAL_MAXIMUM (101)
		    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
		    0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
		    0x29, 0x65,                    //   USAGE_MAXIMUM (Keyboard Application)
		    0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
		    0xc0                           // END_COLLECTION
};

```

set correct size of the `HID_MOUSE_REPORT_DESC` in  `Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc/usbd_hid.h`


```c
#define HID_MOUSE_REPORT_DESC_SIZE                 63U
```

This needs to be repeate if the COBE MX regenerate the code.

