# Mama Keyboard

Quick and dirty keyboard for mama to take control of daughter google meet.

![quick and dirty picture](DOCS/photo_2022-02-24 21.29.11.jpeg)

inspitred by [Use STM32 as a KEYBOARD || F103C8 || USB Device HID by Controllers Tech](https://www.youtube.com/watch?v=tj1_hsQ5PR0&list=WL&index=15)

Using a STM32F411CEUX Chinese blackpill

## Functions

|Raise Hand | Volume Down | Volume Up | Mute Mic |
|:--:|:--:|:--:|:--:|
| Ctrl + ⌘ + h | 👇 | ▲ |⌘ + d |


## Code

specific keycodes at [Key Action](Core/Src/main.c#L179)


## specific USB_HID 

after rebuilding the Cube MX model, we need to correct the keyboard, see [DOCS](DOCS/README.md)

## verify USB connections

```bash
watch -n 1 "ioreg -p IOUSB -w0 | sed 's/[^o]*o //; s/@.*$//' | grep -v '^Root.*'"
```

and check for `mama_keyboard`

