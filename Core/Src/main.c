/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_hid.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
extern USBD_HandleTypeDef hUsbDeviceFS;

typedef struct
{
	uint8_t MODIFIER;
	uint8_t RESERVED;
	uint8_t KEYCODE1;
	uint8_t KEYCODE2;
	uint8_t KEYCODE3;
	uint8_t KEYCODE4;
	uint8_t KEYCODE5;
	uint8_t KEYCODE6;
} keyboardHID;

keyboardHID keyboardhid = {0,0,0,0,0,0,0,0};

/********************* EXTI RELATED ************************/





typedef enum
{
  NO_BUTTON_PRESS = 0,
  UP_BUTTON_PRESS,
  DOWN_BUTTON_PRESS
}button_State;

button_State button_flag_green= NO_BUTTON_PRESS;
button_State button_flag_red=NO_BUTTON_PRESS;
button_State button_flag_blue=NO_BUTTON_PRESS;
button_State button_flag_orange=NO_BUTTON_PRESS;


/* GPIO PIN IRQ Callback */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if (GPIO_Pin == KEY_GREEN_Pin)
	{
		button_flag_green=1;
		if (HAL_GPIO_ReadPin(KEY_GREEN_GPIO_Port, KEY_GREEN_Pin)==GPIO_PIN_RESET )
			button_flag_green=UP_BUTTON_PRESS;
		else
			button_flag_green=DOWN_BUTTON_PRESS;

	}
	if (GPIO_Pin == KEY_RED_Pin)
	{
		button_flag_red=1;
		if (HAL_GPIO_ReadPin(KEY_RED_GPIO_Port, KEY_RED_Pin)==GPIO_PIN_RESET )
			button_flag_red=UP_BUTTON_PRESS;
		else
			button_flag_red=DOWN_BUTTON_PRESS;

	}
	if (GPIO_Pin == KEY_BLUE_Pin)
	{
		button_flag_blue=1;
		if (HAL_GPIO_ReadPin(KEY_BLUE_GPIO_Port, KEY_BLUE_Pin)==GPIO_PIN_RESET )
			button_flag_blue=UP_BUTTON_PRESS;
		else
			button_flag_blue=DOWN_BUTTON_PRESS;

	}
	if (GPIO_Pin == KEY_ORANGE_Pin)
	{
		button_flag_orange=1;
		if (HAL_GPIO_ReadPin(KEY_ORANGE_GPIO_Port, KEY_ORANGE_Pin)==GPIO_PIN_RESET )
			button_flag_orange=UP_BUTTON_PRESS;
		else
			button_flag_orange=DOWN_BUTTON_PRESS;

	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  uint8_t button_down_red=0;
  uint8_t button_down_blue=0;

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();
  /* USER CODE BEGIN 2 */
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 0);
  HAL_Delay (500);
  HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, 1);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  /* USER CODE BEGIN 3 */
	  // GREEN KEY Action => ⌘ + d => Mute/unMute Mic
	  if (button_flag_green == DOWN_BUTTON_PRESS)
	  {
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 0); // Board LED on
		  keyboardhid.MODIFIER = 0x08;  //  `⌘` 
		  keyboardhid.KEYCODE1 = 0x07;  //  `d`
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)(&keyboardhid), sizeof (keyboardhid));
		  button_flag_green=NO_BUTTON_PRESS;
	  }
	  if (button_flag_green == UP_BUTTON_PRESS)
	  {
		  keyboardhid.MODIFIER = 0x00;  // shift release
		  keyboardhid.KEYCODE1 = 0x00;  // release key
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&keyboardhid, sizeof (keyboardhid));
		  button_flag_green=NO_BUTTON_PRESS;
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 1);  // Toggle the Key LED
		  HAL_GPIO_TogglePin(LED_KEY_GREEN_GPIO_Port, LED_KEY_GREEN_Pin); / Board LED off
	  }

	  // RED KEY Action => Volume Up
	  if (button_flag_red == DOWN_BUTTON_PRESS)
	  {
		  HAL_GPIO_WritePin(LED_KEY_RED_GPIO_Port, LED_KEY_RED_Pin,0);
		  button_down_red=1;
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 0);
		  keyboardhid.MODIFIER = 0x00;  // left GUI
		  keyboardhid.KEYCODE1 = 0x80;  // volume Up
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)(&keyboardhid), sizeof (keyboardhid));
		  button_flag_red=NO_BUTTON_PRESS;

	  }
	  if (button_flag_red == UP_BUTTON_PRESS)
	  {
		  keyboardhid.MODIFIER = 0x00;  // shift release
		  keyboardhid.KEYCODE1 = 0x00;  // release key
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&keyboardhid, sizeof (keyboardhid));
		  button_flag_red=NO_BUTTON_PRESS;
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 1);
		  HAL_GPIO_WritePin(LED_KEY_RED_GPIO_Port, LED_KEY_RED_Pin,1);
		 button_down_red=0;
	  }

	  // BLUE KEY Action => Volume Down
	 if (button_flag_blue == DOWN_BUTTON_PRESS)
	 {
	 	button_down_blue=1;
	 	HAL_GPIO_WritePin(LED_KEY_BLUE_GPIO_Port, LED_KEY_BLUE_Pin,0);
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 0);
		keyboardhid.MODIFIER = 0x00;  // left GUI
		keyboardhid.KEYCODE1 = 0x81;  // volume down
		USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)(&keyboardhid), sizeof (keyboardhid));
		button_flag_blue=NO_BUTTON_PRESS;


	 }
     if (button_flag_blue == UP_BUTTON_PRESS)
	 {
		keyboardhid.MODIFIER = 0x00;  // shift release
		keyboardhid.KEYCODE1 = 0x00;  // release key
		USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&keyboardhid, sizeof (keyboardhid));
		button_flag_blue=NO_BUTTON_PRESS;
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 1);
		HAL_GPIO_WritePin(LED_KEY_BLUE_GPIO_Port, LED_KEY_BLUE_Pin,1);
		button_down_blue=0;
	  }

	 	//orange Key Action => Ctrl + ⌘ + h => raise/lower  hand for zoom
	  if (button_flag_orange == DOWN_BUTTON_PRESS)
	  {
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 0);
		  keyboardhid.MODIFIER = 0x81;  // `Ctrl + ⌘ `
		  keyboardhid.KEYCODE1 = 0x0B;  // `h`
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&keyboardhid, sizeof (keyboardhid));
		  button_flag_orange=NO_BUTTON_PRESS;
	  }
	  if (button_flag_orange == UP_BUTTON_PRESS)
	  {
		  keyboardhid.MODIFIER = 0x00;  // modifier  release
		  keyboardhid.KEYCODE1 = 0x00;  // release key
		  USBD_HID_SendReport(&hUsbDeviceFS, (uint8_t *)&keyboardhid, sizeof (keyboardhid));
		  button_flag_orange=NO_BUTTON_PRESS;
		  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, 1);
		  HAL_GPIO_TogglePin(LED_KEY_ORANGE_GPIO_Port, LED_KEY_ORANGE_Pin);
	  }
    
    // FLASH Volume KEY LED while pressed down
	  if (button_down_red) HAL_GPIO_TogglePin(LED_KEY_RED_GPIO_Port, LED_KEY_RED_Pin);
	  if (button_down_blue) HAL_GPIO_TogglePin(LED_KEY_BLUE_GPIO_Port, LED_KEY_BLUE_Pin);
	  
    // Sleep in between 100ms
    HAL_Delay (100);
	  /* USER CODE END 3 */
  }
    /* USER CODE END WHILE */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 15;
  RCC_OscInitStruct.PLL.PLLN = 144;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 5;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LED_KEY_GREEN_Pin|LED_KEY_RED_Pin|LED_KEY_BLUE_Pin|LED_KEY_ORANGE_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : LED_Pin */
  GPIO_InitStruct.Pin = LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LED_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : KEY_GREEN_Pin KEY_RED_Pin KEY_BLUE_Pin KEY_ORANGE_Pin */
  GPIO_InitStruct.Pin = KEY_GREEN_Pin|KEY_RED_Pin|KEY_BLUE_Pin|KEY_ORANGE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LED_KEY_GREEN_Pin LED_KEY_RED_Pin LED_KEY_BLUE_Pin LED_KEY_ORANGE_Pin */
  GPIO_InitStruct.Pin = LED_KEY_GREEN_Pin|LED_KEY_RED_Pin|LED_KEY_BLUE_Pin|LED_KEY_ORANGE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI1_IRQn);

  HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI2_IRQn);

  HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

